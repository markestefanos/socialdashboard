import {observable} from 'mobx';

module.exports = observable({
    tags: [""],
    users: [""],
    posts: [],
    sort: 'age',
    isLoading: false,
    requested: {
        tags: [],
        users: [],
        userMaxIDs: [],
        tagMaxIDs: []
    },
})