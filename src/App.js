import React from 'react';
import ReactDOM from 'react-dom';
import actions from './actions/searchActions'
import {observer} from 'mobx-react';
import store from './store'
import instagram from './instagram'
import Dropdown from 'react-dropdown'
import Select from 'react-select'
import DevTools, { configureDevtool } from 'mobx-react-devtools';

configureDevtool({
  logEnabled: true,
});

actions.getSelfPosts();

export default class App extends React.Component {
  render() {
    return (
      <Dashboard/>
    );
  }
}


@observer
class Sidebar extends React.Component {
  render() {
    return (
    <div className = 'sidebar'>
          <Sort/>
          <Hashtags icon = "#" title = "Hashtags" 
          data = {store.tags} 
          editAction = {actions.editTag} 
          addAction = {actions.addTag}
          removeAction = {actions.removeTag}/>
          <Hashtags icon = "@" title = "Usernames" 
          data = {store.users} 
          editAction = {actions.editUser} 
          addAction = {actions.addUser}
          removeAction = {actions.removeUser}/>
      </div>
    );
  }
};

@observer
class Dashboard extends React.Component {
  render() {
    return (
      <div className ='container'>
        <DevTools/>
        <Sidebar/>
        <div className = 'instafeed'>
          <Instafeed />
        </div>
      </div>
    )
  }
}

// <SimpleSelect 
//       options = {options} 
//       placeholder = "Sort By" 
//       theme = "material" 
//       onValueChange={actions.sortBy}/>


class Sort extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: "Sort By"
    }
  }
  change =  (val) => {
    this.setState({value: val})
    actions.sortBy(val)
  }
  render() {
    var options = [
      { value: 'age', label: 'Age' },
      { value: 'likes', label: 'Likes' }];
    return (
      <div className = 'sort'>
        <Select
        value={this.state.value}
        clearable = {false}
        options={options}
        onChange={this.change}
        />
    </div>
    )
  }
}

@observer

class Hashtags extends React.Component {

  constructor() {
    super();
    this.state = {
      expanded: false
    };
  }
  click = () => {
    this.setState({expanded: !this.state.expanded})
  }
  render() {
    var body = "";
    if (this.state.expanded) {
      body = (
        <div className = 'keywordMenu '>
          {this.props.data.map((x, i) =>
          <input
              type="text"
              key = {i}
              value={x}
              onChange={(event) => {
                this.props.editAction(i, event.target.value)
              }}
            />
          )}
          <div className = 'searchContainer'>
            <div className = 'incrementButton' onClick = {this.props.addAction }  >
              <img src = '../img/plus.png' />
            </div>
            <div className = 'incrementButton' onClick = {this.props.removeAction }  >
              <img src = '../img/minus.png' />
            </div>
            <div style = {{margin: 'auto'}}/>
              <div className = 'searchButton unselectable' onClick= {actions.search} >
                Search
              </div>
            </div>
          </div>)
    }
    return (
      <div className = "keywordMenu" >
        <div className = "categoryHeader unselectable" onClick = {this.click} >
          <div className = 'icon'>
            {this.props.icon}
          </div>
          {this.props.title}
          <div style = {{margin: 'auto'}}/>
          <div className = {'caret' + (this.state.expanded ? " rotate" : "")} >
            <img  src = '../img/caret.png' />
          </div>
        </div>
      {body}
      </div>
    )
  }
}

class Input extends React.Component {
  render() { return (
    <input
          type="text"
          value={this.state.value}
          onChange={this.props.onChange}
  />
  )
}
}

@observer
class Instafeed extends React.Component {
  constructor(props) {
    super(props)
    window.addEventListener("scroll", this.handleScroll);
  }

  handleScroll(e){
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;
    if (windowBottom >= docHeight) {
      actions.paginate()
    }
  }

  render() {
    return (
      <div>
      {store.posts.map((x, i) =>
    <Instapost key={i} data = {x}/>
    )}
    </div>
    )
  }
}


var Instapost = React.createClass({
  render: function() {
    var data = this.props.data
    return (
    <div className = 'instapost'>
      <Image image = {data.images.low_resolution.url}/>
      <Likebar/>
      <div className = 'username'>
        @{data.user.username}
      </div>
      <div className = 'date'>
      {timestampToDate(data.created_time)}
      </div>
      <div className = 'description'>
      {data.caption != null ? data.caption.text : ""}
      </div>
    </div>
    );
  }
})

var timestampToDate = (timestamp) => {
  var date = new Date(timestamp*1000)
  var year = date.getFullYear()
  var month = date.getMonth()
  var day = date.getDate()
  return (month + '/' + day + "/" + year)
}

class Details extends React.Component {

}



class Likebar extends React.Component {
  constructor() {
    super();
    this.state = {
      like: 0
    };
  }
  likePressed() {
    console.log("like")
    this.setState({like: this.state.like == 1 ? 0 : 1})
  }
  unlikePressed() {
    this.setState({like: this.state.like == -1 ? 0 : -1})
  }
  render() {
    return (
      <div className = 'buttonContainer'>
      <LikeButton selectedColor= {'#D1F1C7'} onClick= {() => this.likePressed()} selected = {this.state.like == 1}>
          <img src = '../img/check.png' />
      </LikeButton>
      <div className = 'buttonSeparator'/>
      <LikeButton selectedColor= {'#FFB7B2'} onClick= {() => this.unlikePressed()} selected = {this.state.like == -1}>
          <img src = '../img/x.png' />
      </LikeButton>
      </div>
    );
  }
}

class LikeButton extends React.Component {
  render() {
    return (
      <div className = 'button'onClick={()=> this.props.onClick()} style = {this.props.selected ? {backgroundColor: this.props.selectedColor} : {backgroundColor: 'white'}}>
        {this.props.children}
      </div>
    );
  }
}

var Image = React.createClass({
  render: function() {
    return (
    <div className = 'image'>
       <img src={this.props.image}/>
    </div>
    );
  }
});

var Images = () => {
  return this.state.posts.map((x, i) =>
    <Instapost key={i + 1} data = {x}/>
  )
}