import store from '../store'
import { observable, computed, action } from 'mobx';
import instagram from '../instagram'


class Actions {

  @action addTag() {
    store.tags.push("")
  }

  @action removeTag() {
    store.tags.pop()
  }

  @action sortBy(sort) {
    if (sort != "likes" && sort != "age") return
    store.sort = sort
    store.posts = instagram.sort(store.posts, sort)
    console.log(store)
  }

  @action addUser() {
    store.users.push("")
  }

  @action removeUser() {
    store.users.pop()
  }

  @action editTag(index, text) {
    store.tags[index] = text
  }

  @action editUser(index, text) {
    store.users[index] = text
  }

  @action addPosts(posts) {
    store.posts = store.posts.concat(posts)
  }


  @action search() {
    instagram.search(store.tags, store.users).then(action(data => {
      store.isLoading = false
      store.requested.tags = store.tags
      store.requested.users = store.users
      store.requested.tagMaxIDs = data.tagMaxIDs
      store.requested.userMaxIDs = data.userMaxIDs
      var sorted = instagram.sort(data.data, store.sort)
      store.posts = sorted
      console.log("store updated. Store:")
      console.log(store)
    }))
  }


  @action getSelfPosts(maxID = 0) {
    store.isLoading = true
    instagram.getSelfPosts(maxID).then(action(data => {
      store.isLoading = false
      store.requested.users = ["self"]
      store.requested.userMaxIDs[0] = data.maxID
      store.posts = store.posts.concat(data.data)
      console.log("store updated. Store:")
      console.log(store)
    }))
  }

  @action paginate() {
    if (store.isLoading) return
    if (store.requested.users[0] == "self") {
      this.getSelfPosts(store.requested.userMaxIDs[0])
    } else {
      console.log('paginate')
      store.isLoading = true
      instagram.search(store.tags.slice(0), store.users.slice(0), store.requested.tagMaxIDs.slice(0), store.requested.userMaxIDs.slice(0)).then(action(data => {
        store.isLoading = false
        store.requested.tagMaxIDs = data.tagMaxIDs
        store.requested.userMaxIDs = data.userMaxIDs
        store.posts = store.posts.concat(data.data)
      }))
    }
  }
}

module.exports = new Actions()