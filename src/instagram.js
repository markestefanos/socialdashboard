var token = '1346616636.54da896.b174c31877a34f5b924746180a0bb42e',
    num_photos = 32,
    requestcap = 5,
    requests = 0,
    selfPostMaxID,
    usernameSearchM;



var instagram = {
    getSelfPosts(maxID = 0) {
        return $.ajax({
            url: 'https://api.instagram.com/v1/users/self/media/recent',
            dataType: 'jsonp',
            type: 'GET',
            data: {access_token: token, count: num_photos, max_id: maxID}
        }).then((data) => {
            data.maxID = data.pagination.next_max_id
            return data
        }).fail((data) => {
            console.log(data);
            return data
        })
    },

    searchTag(hashtag, maxID = 0) {
        return $.ajax({
            url: 'https://api.instagram.com/v1/tags/' + hashtag + '/media/recent',
            dataType: 'jsonp',
            type: 'GET',
            data: {access_token: token, count: num_photos, max_tag_id: maxID},
        }).then((data) => {
            data.maxID = data.pagination.next_max_id
            return data
        }).fail((data) => {
			console.log("searchTag failed")
            console.log(data);
            return data
        });
    },

	searchTags(tags, maxIDs = []) {
		tags = tags.filter((x) => x!="")
		if (maxIDs.length == 0) maxIDs = tags.map(x => 0)
		return Promise.all(tags.map((x, i)=>this.searchTag(x, maxIDs[i]))).then((values => {
			const maxIDs = values.map(data => data.maxID)
			const posts = values.reduce((prev, data) => prev.concat(data.data), [])
			const sorted = this.sort(posts)
			return {data: sorted, maxIDs}
		}))
	},

	searchUser(username, maxID = 0) {
		return $.ajax({
			url: 'https://api.instagram.com/v1/users/search',
			dataType: 'jsonp',
			type: 'GET',
			data: {access_token: token, q: username}
		}).then((data) => {
			return $.ajax({
				url: 'https://api.instagram.com/v1/users/' + data.data[0].id + '/media/recent', 
				dataType: 'jsonp',
				type: 'GET',
				data: {access_token: token, count: num_photos, max_id: maxID}
			});
		}).then((data) => {
			data.maxID = data.data.slice(-1)[0].id
			return data
		}).fail((data) => {
			console.log(data);
			return data
		});
	},

	searchUsers(usernames, maxIDs = []) {
		usernames = usernames.filter(x => x!="")
		if (maxIDs.length == 0) { maxIDs = usernames.map(x => 0) }
		return Promise.all(usernames.map((x, i)=>this.searchUser(x, maxIDs[i]))).then((values => {
			const maxIDs = values.map(data => data.maxID)
			const posts = values.reduce((prev, data) => prev.concat(data.data), [])
			const sorted = this.sort(posts)
			return {data: sorted, maxIDs}
		}))
	},

	search(tags = [], usernames = [], tagMaxIDs = [], userMaxIDs = []) {
		return Promise.all([this.searchTags(tags, tagMaxIDs), this.searchUsers(usernames, userMaxIDs)]).then(values => {
			const data = this.sort(values[0].data.concat(values[1].data))
			return {data, tagMaxIDs: values[0].maxIDs, userMaxIDs: values[1].maxIDs}
		})
	},

	sort(data, sortBy = 'age') {
	if (sortBy == 'likes') {
		return data.sort((a, b) => {
			return b.likes.count - a.likes.count
		})
	} else if (sortBy == 'age') {
		return data.sort((a, b) => {
			return b.created_time - a.created_time
		})
	} else { //ML

	}
}
}

module.exports = instagram
